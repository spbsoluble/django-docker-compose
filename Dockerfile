FROM python:3.9

ENV PYTHONUNBUFFERED 1
RUN mkdir -p /opt/services/djangoapp/src

COPY Pipfile Pipfile.lock /opt/services/djangoapp/src/
WORKDIR /opt/services/djangoapp/src
RUN pip install pipenv && pipenv install --system

COPY . /opt/services/djangoapp/src
RUN cd django_project && python manage.py collectstatic --no-input
#RUN /opt/services/djangoapp/src/config/nginx/generate_cert.sh && \
#    mkdir -p /etc/ssl/certs && \
#    mkdir -p /etc/ssl/private && \
#    mv django_server.crt /etc/ssl/certs/ && \
#    mv django_server.key /etc/ssl/private/
EXPOSE 8443
CMD ["gunicorn", "-c", "config/gunicorn/conf.py", "--bind", "0.0.0.0:8443", "--chdir", "django_project", "django_project.wsgi:application"]
