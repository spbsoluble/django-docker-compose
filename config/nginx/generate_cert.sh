#!/usr/bin/env bash
export SERVER_NAME="django_server"
#openssl genpkey -out "${SERVER_NAME}.key" -algorithm RSA -pkeyopt rsa_keygen_bits:2048
#chmod 600 "${SERVER_NAME}.key"
#openssl req -new -key "${SERVER_NAME}.key" -out "${SERVER_NAME}.csr" -subj "/C=US/ST=CA/L=LA/O=XCorp Inc./OU=X/CN=${SERVER_NAME}"
#chmod 600 "${SERVER_NAME}.csr"
#openssl req -text -in "${SERVER_NAME}.csr" -noout
#openssl x509 -req -days 365 -in "${SERVER_NAME}.csr" -signkey "${SERVER_NAME}.key" -out "${SERVER_NAME}.crt"
#chmod 644 "${SERVER_NAME}.crt"

openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout "${SERVER_NAME}.key" -out "${SERVER_NAME}.crt" -config localhost.conf

